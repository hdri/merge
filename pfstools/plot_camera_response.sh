#!/bin/sh

# TODO: parameter count check

INPUT_RESPONSE="$1"
OUTPUT_PNG=$(basename "$INPUT_RESPONSE").png


# Extract individual channels from the computed camera response
sed -n '1,262p' "$INPUT_RESPONSE" > "$INPUT_RESPONSE".red
sed -n '264,525p' "$INPUT_RESPONSE" > "$INPUT_RESPONSE".green
sed -n '527,788p' "$INPUT_RESPONSE" > "$INPUT_RESPONSE".blue
sed -n '790,1051p' "$INPUT_RESPONSE" > "$INPUT_RESPONSE".weight


# Generate the plotting script and plot
echo "
#set term postscript enhanced color
#set output 'OUTPUTFILE.eps'
set terminal png truecolor size 1024,1024 enhanced font 'Verdana,10'
set output '$OUTPUT_PNG'
#set datafile separator ' '
#set terminal svg size 1024,1024 fname 'Verdana' fsize 10
#set output 'OUTPUTFILE.svg'
#set title 'OUTPUTFILE'
#set style fill transparent solid 0.02 noborder
#set style circle radius 0.005
#set xrange [0:10]
#set yrange [0:10]
set xrange [0:255]
set yrange [0:10]
#set view equal xy
#set size square
#set size ratio -1
#set logscale xy
plot '$INPUT_RESPONSE.red' using 2:3 with lines lc rgb 'red',\
	 '$INPUT_RESPONSE.green' using 2:3 with lines lc rgb 'green',\
	 '$INPUT_RESPONSE.blue' using 2:3 with lines lc rgb 'blue',\
	 '$INPUT_RESPONSE.weight' using 2:1 with lines lc rgb 'black'
#plot '$INPUT_CSV' using 1:2 with circles lc rgb 'black', x**1
" | gnuplot


# Delete temporary responses
rm "$INPUT_RESPONSE".red "$INPUT_RESPONSE".green "$INPUT_RESPONSE".blue "$INPUT_RESPONSE".weight
