#!/bin/sh

PFSTOOLS_DOCKER_IMAGE='pfstools_tmp'

DIR="$(dirname "$(realpath "$0")")"

set -e
set -o pipefail

#set -x

args_to_lines() {
	for ARG in "$@"; do
		echo "$ARG"
	done
}

if [ $# -lt 2 ]; then
	echo 'Usage: $0 merged_output.hdr input1.jpg [input2.jpg] [...]'
	exit 1
fi

OUTPUT_HDR="$1"
shift

# Prepare image metadata (exposure time, ISO, ...)
args_to_lines "$@" | tr '\n' '\0' | docker run -i --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -x -e -c "
	xargs -0 jpeg2hdrgen > '$OUTPUT_HDR'.hdrgen
"


# Compute camera response function
args_to_lines "$@" | tr '\n' '\0' | docker run -i --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -x -e -c "set -o pipefail;
	pfsinhdrgen '$OUTPUT_HDR'.hdrgen |
	pfshdrcalibrate -v -s '$OUTPUT_HDR'.response > /dev/null
"


# Merge the images using the computed CRF
args_to_lines "$@" | tr '\n' '\0' | docker run -i --rm -v "$(pwd)":/workdir "$PFSTOOLS_DOCKER_IMAGE" bash -x -e -c "set -o pipefail;
	pfsinhdrgen '$OUTPUT_HDR'.hdrgen |
	pfshdrcalibrate -v -f '$OUTPUT_HDR'.response |
	pfsoutexr '$OUTPUT_HDR'.exr
"


# Remove temporary image metadata
rm "$OUTPUT_HDR".hdrgen


# Plot the CRF
"$DIR"/plot_camera_response.sh "$OUTPUT_HDR".response
