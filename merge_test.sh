#!/bin/sh

DIR="$(dirname "$(realpath "$0")")"

set -e
set -x

merge() {
	HDR_NAME="$1"
	shift
	"$DIR"/OpenCV/merge.sh "${HDR_NAME}_OpenCV" "$@"
	"$DIR"/pfstools/merge.sh "${HDR_NAME}_pfstools.hdr" "$@"
}

cd "$DIR"/data

merge IMG_7652+ IMG_7652.JPG IMG_7653.JPG # IMG_7654.JPG was completely white = overexposed, IMG_7655.JPG was completely black for an unknown reason
merge okno3_without_saturation_bleeding3 okno3_without_saturation_bleeding3/*
merge okno3_without_saturation_bleeding2 okno3_without_saturation_bleeding2/*
merge okno3_without_saturation_bleeding okno3_without_saturation_bleeding/*
merge okno3 okno3/*.JPG
merge okno2 okno2/IMG_7886.JPG okno2/IMG_7887.JPG okno2/IMG_7888.JPG okno2/IMG_7889.JPG okno2/IMG_7890.JPG okno2/IMG_7891.JPG okno2/IMG_7892.JPG okno2/IMG_7893.JPG okno2/IMG_7894.JPG okno2/IMG_7895.JPG okno2/IMG_7896.JPG okno2/IMG_7897.JPG okno2/IMG_7898.JPG okno2/IMG_7899.JPG okno2/IMG_7900.JPG
merge okno okno/IMG_7852.JPG okno/IMG_7853.JPG okno/IMG_7854.JPG okno/IMG_7855.JPG okno/IMG_7856.JPG okno/IMG_7857.JPG okno/IMG_7858.JPG okno/IMG_7859.JPG okno/IMG_7860.JPG okno/IMG_7861.JPG okno/IMG_7862.JPG okno/IMG_7863.JPG okno/IMG_7864.JPG okno/IMG_7865.JPG okno/IMG_7866.JPG okno/IMG_7867.JPG okno/IMG_7868.JPG okno/IMG_7869.JPG okno/IMG_7870.JPG okno/IMG_7871.JPG okno/IMG_7872.JPG okno/IMG_7873.JPG okno/IMG_7874.JPG okno/IMG_7875.JPG okno/IMG_7876.JPG okno/IMG_7877.JPG okno/IMG_7878.JPG okno/IMG_7879.JPG okno/IMG_7880.JPG okno/IMG_7881.JPG okno/IMG_7882.JPG okno/IMG_7883.JPG
merge StLouisArchMultExp+.hdr StLouisArchMultExpEV+4.09.JPG StLouisArchMultExpEV+1.51.JPG StLouisArchMultExpEV-1.82.JPG StLouisArchMultExpEV-4.72.JPG
merge IMG_7573+ IMG_7573.JPG IMG_7574.JPG IMG_7575.JPG IMG_7576.JPG # !!! non-constant ISO
merge IMG_7620+ IMG_7620.JPG IMG_7621.JPG IMG_7622.JPG IMG_7623.JPG
merge IMG_7624+ IMG_7624.JPG IMG_7625.JPG IMG_7626.JPG IMG_7627.JPG
merge IMG_7635+ IMG_7635.JPG IMG_7636.JPG IMG_7637.JPG IMG_7638.JPG
merge IMG_7644+ IMG_7644.JPG IMG_7645.JPG IMG_7646.JPG IMG_7647.JPG
merge IMG_7648+ IMG_7648.JPG IMG_7649.JPG IMG_7650.JPG IMG_7651.JPG
merge IMG_7652+ IMG_7652.JPG IMG_7653.JPG # IMG_7654.JPG was completely white = overexposed, IMG_7655.JPG was completely black for an unknown reason
merge IMG_7656+ IMG_7656.JPG IMG_7657.JPG IMG_7658.JPG IMG_7659.JPG
