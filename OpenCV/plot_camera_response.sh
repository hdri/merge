#!/bin/sh

# TODO: parameter count check

INPUT_CSV="$1"
OUTPUT_PNG=$(basename "$INPUT_CSV").png

echo "
#set term postscript enhanced color
#set output 'OUTPUTFILE.eps'
set terminal png truecolor size 1024,1024 enhanced font 'Verdana,10'
set output '$OUTPUT_PNG'
set datafile separator ','
#set terminal svg size 1024,1024 fname 'Verdana' fsize 10
#set output 'OUTPUTFILE.svg'
#set title 'OUTPUTFILE'
#set style fill transparent solid 0.02 noborder
#set style circle radius 0.005
#set xrange [0:10]
#set yrange [0:10]
set xrange [0:255]
set yrange [0:10]
#set view equal xy
#set size square
#set size ratio -1
#set logscale xy
plot '$INPUT_CSV' using 1:2 with lines lc rgb 'red',\
	 '$INPUT_CSV' using 1:3 with lines lc rgb 'green',\
	 '$INPUT_CSV' using 1:4 with lines lc rgb 'blue'
#plot '$INPUT_CSV' using 1:2 with circles lc rgb 'black', x**1
" | gnuplot

