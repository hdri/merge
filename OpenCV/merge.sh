#!/bin/sh

DIR="$(dirname "$(realpath "$0")")"

set -e
set -o pipefail
set -u

set -x

if [ $# -lt 2 ]; then
	echo 'Usage: $0 merged_output_name input1.jpg [input2.jpg] [...]'
	exit 1
fi

get_exposure_time() {
	FILENAME="$1"
	EXTENSION="$(echo "$FILENAME" | sed 's/.*\.//')"
	EXTENSION_LOWER="$(echo "$EXTENSION" | tr '[:upper:]' '[:lower:]')"
	if [ "$EXTENSION_LOWER" == 'jpg' ]; then
		exiftool -ShutterSpeed "$1" | sed 's/.*: //' | bc -l
		# TODO: factor ISO into the exposure time?
	else
		# The file should be named "something_[exposure_time].[extension]" - extract the exposure_time
		# (grep should return non-zero if the filename does not contain exposure time)
		basename "$FILENAME" ."$EXTENSION" | sed 's/.*_//' | grep '[0-9]\.[0-9][0-9]*'
	fi
}

OUTPUT_HDR_BASENAME="$1"
shift

ARGUMENTS=''

for INPUT_FILE in "$@"; do
    EXPOSURE_TIME="$(get_exposure_time "$INPUT_FILE")"
	echo $INPUT_FILE - exposure time: $EXPOSURE_TIME
	ARGUMENTS="$ARGUMENTS
$INPUT_FILE
$EXPOSURE_TIME"
done

# Merge!
echo "$ARGUMENTS" | tr '\n' '\0' | xargs -0 python "$DIR"/merge.py --method debevec "$OUTPUT_HDR_BASENAME"_debevec
echo "$ARGUMENTS" | tr '\n' '\0' | xargs -0 python "$DIR"/merge.py --method robertson "$OUTPUT_HDR_BASENAME"_robertson

# Plot the camera response functions
echo 'Plotting camera response functions'
"$DIR"/plot_camera_response.sh "$OUTPUT_HDR_BASENAME"_debevec_camera_response.csv
"$DIR"/plot_camera_response.sh "$OUTPUT_HDR_BASENAME"_robertson_camera_response.csv
