import cv2
import numpy as np
import csv
import sys

# Merge a set of given (low dynamic range) images with given exposure times into one HDR image, either computing camera response function or using a provided CRF

# Usage: merge.py --method [debevec|robertson] [--use_response CRF.csv] output_hdr_basename input1.jpg exposure_time_of_input1_in_seconds [input2.jpg exposure_time] [...]

response_fname = None

calibrate_method = None
merge_method = None

# Parse options
while sys.argv[1].startswith('--'):
    if sys.argv[1] == '--use_response':
        response_fname = sys.argv[2]

    elif sys.argv[1] == '--method':
        if sys.argv[2] == 'debevec':
            calibrate_method = cv2.createCalibrateDebevec()
            merge_method = cv2.createMergeDebevec()
        elif sys.argv[2] == 'robertson':
            calibrate_method = cv2.createCalibrateRobertson()
            merge_method = cv2.createMergeRobertson()

    # "shift by 2"
    sys.argv = sys.argv[2:]

if calibrate_method is None:
    print('"--method [debevec|robertson]" is required')
    exit()

print(sys.argv)


output_hdr_basename = sys.argv[1]

img_fn = []
exposure_times = []

for i in range(2, len(sys.argv), 2):
    print(sys.argv[i], sys.argv[i + 1])
    img_fn.append(sys.argv[i])
    exposure_times.append(sys.argv[i + 1])

img_list = [cv2.imread(fn) for fn in img_fn]

# TODO: linearize sRGB when loading LDR images?

exposure_times = np.array(exposure_times, dtype=np.float32)

#print('exposure_times: ', exposure_times)

print("Aligning images...")
alignMTB = cv2.createAlignMTB()
alignMTB.process(img_list, img_list)

if response_fname is not None:
    print("Reading Camera Response Function from file...")
    with open(response_fname, 'r') as csvfile:
        reader = csv.reader(csvfile)

        response = np.ndarray(shape=(256, 1, 3), dtype=np.float32)
        i = -1
        for row in reader:
            if i >= 0: # skip header
                response[i][0] = [row[3], row[2], row[1]]
            i += 1
else:
    print("Calculating Camera Response Function...")
    print(img_fn)
    response = calibrate_method.process(img_list, exposure_times) # type: np.ndarray


    print("Writing CRF to csv...")
    with open(output_hdr_basename + '_camera_response.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)

        writer.writerow(['measured', 'calibrated_r', 'calibrated_g', 'calibrated_b'])

        for i in range(0, 256):
            r = response[i][0]
            writer.writerow([i, r[2], r[1], r[0]]) # BGR to RGB

# Merge exposures to HDR image
print("Merging...")
merged = merge_method.process(img_list, times=exposure_times.copy(), response=response)

print("Writing output...")
cv2.imwrite(output_hdr_basename + '.hdr', merged / 255)
#cv2.imwrite(output_hdr_basename + '.jpg', merged)


#tonemap1 = cv2.createTonemapDurand(gamma=2.2)

#res_debvec = tonemap1.process(hdr_debvec.copy())

#res_debvec_8bit = np.clip(res_debvec*255, 0, 255).astype('uint8')
